import { PhoneNumberFormat, PhoneNumberUtil } from 'google-libphonenumber';

type fnArgs = {
  rawPhoneNumber: string;
};

type fnReturn = {
  inputPhoneNumber: string;
  e164PhoneNumber: string;
};

/**
 * Process a phone number string to E.164 format.
 *
 * @param fnArgs
 * @returns
 */
export function main({ rawPhoneNumber }: fnArgs): fnReturn {
  if (rawPhoneNumber === undefined) {
    throw new Error('Input [rawPhoneNumber] missing.');
  }

  console.log(rawPhoneNumber);

  let phoneUtil = new PhoneNumberUtil();
  let parsedPhoneNum = phoneUtil.parseAndKeepRawInput(rawPhoneNumber);
  let intlFmtPhoneNum = phoneUtil.format(parsedPhoneNum, PhoneNumberFormat.E164);

  console.log('Formatted ' + parsedPhoneNum.getRawInput() + ' > ' + intlFmtPhoneNum);

  return { inputPhoneNumber: rawPhoneNumber, e164PhoneNumber: intlFmtPhoneNum };
}
