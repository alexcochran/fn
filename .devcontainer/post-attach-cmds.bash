#!/bin/bash

# Extension script containing shell commands that are run once the container has been started.

# shellcheck source=/dev/null
source /home/vscode/.bashrc

# Install Digital Ocean's doctl
# https://github.com/digitalocean/doctl?tab=readme-ov-file#downloading-a-release-from-github
curl -sL https://github.com/digitalocean/doctl/releases/download/v1.101.0/doctl-1.101.0-linux-amd64.tar.gz | tar -xzv
sudo mv ./doctl /usr/local/bin
doctl auth init --access-token ${DO_API_TOKEN}

# Install Node packages
pnpm install
