# alexcochran\/fn

Utility endpoints and automation helpers implemented as cloud functions for use anywhere, anytime.

This project uses a [Dev Container](https://code.visualstudio.com/docs/devcontainers/containers).

## Development

### Configuration

#### [`.devcontainer/devcontainer.env`](.devcontainer/devcontainer.env)

This configuration is limited to the development environment managed by the Dev Container configuration.

...

## Resources

...

---

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

---
